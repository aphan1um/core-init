DROP MATERIALIZED VIEW IF EXISTS viewdataset;

CREATE MATERIALIZED VIEW IF NOT EXISTS viewdataset AS
SELECT jobdatum.group_id,
       anzsco_id,
       infoeducationlevel.title AS education_level,
       infojobtype.jobtype,
       jobwages.low_wage AS wage_low,
       jobwages.high_wage AS wage_high,
       infowagetypes.title AS wage_type,
       jobexperience.years_needed AS experience_years,
       jobdatum.date_listed AS date_listed,
       state_code AS state,
       firm_type AS company_firm_type,
       anzsic_division AS company_anzsic_division,
       employee_count AS company_employee_count,
       jobdatum.age_min,
       jobdatum.age_max,
       COALESCE("gender-implicit-masculine", 0) AS "count_masculine_phrases",
       COALESCE("gender-implicit-feminine", 0) AS "count_feminine_phrases",
       COALESCE("disability-general", 0) AS "count_ableist_phrases"
FROM jobdatum
INNER JOIN jobgroups USING (group_id)
INNER JOIN companies USING (company_id)
LEFT JOIN infoabrentities USING (entity_id)
LEFT JOIN infoeducationlevel USING (edulevel_id)
LEFT JOIN infojobtype USING (jobtype_id)
LEFT JOIN jobwages USING (group_id)
LEFT JOIN infowagetypes ON (infowagetypes.wagetype_id = jobwages.wagetype_id)
LEFT JOIN jobexperience USING (group_id)
LEFT JOIN viewdiscrimcounts ON (viewdiscrimcounts.group_id = jobdatum.group_id)
WHERE jobdatum."version" =
    (SELECT MAX(jobdatum."version")
     FROM jobdatum);
