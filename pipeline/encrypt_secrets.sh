#!/bin/bash

kubectl create secret generic $(yq e '.global.config.secrets_name' values.yaml) --from-env-file=.secrets --dry-run=client -o=yaml | kubeseal --scope=cluster-wide > ./templates/secrets-svc-sealed.json
kubectl create secret --namespace=jobs-kaniko docker-registry dockercred $(cat .secrets_docker | sed -rz 's/(^|\n)/ --/g') --dry-run=client -o=yaml | kubeseal --scope=cluster-wide > ./templates/secrets-kaniko-sealed.json
