#!/bin/bash

helm upgrade --install --timeout=45m --set-file global._secrets='./.secrets' pipeline . $@