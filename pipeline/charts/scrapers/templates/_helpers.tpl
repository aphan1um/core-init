{{- define "scraper-pod-template" }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Chart.Name }}-{{ .scraper_name }}
  labels:
    type: scrapers
    name: {{ .scraper_name }}
  annotations:
    rollme: {{ .rollmeValue | trim }}
spec:
  replicas: 1
  revisionHistoryLimit: 0
  strategy:
    type: Recreate
  selector:
    matchLabels:
      type: scrapers
      app: {{ .scraper_name }}
  template:
    metadata:
      labels:
        type: scrapers
        app: {{ .scraper_name }}
      annotations:
        rollme: {{ .rollmeValue | trim }}
    spec:
      initContainers:
        - name: git-sync-init
          image: k8s.gcr.io/git-sync/git-sync:v3.3.4
          env:
            - name: GIT_SYNC_REPO
              value: {{ .Values.repo.location | squote }}
            - name: GIT_SYNC_BRANCH
              value: {{ .Values.repo.branch }}
            - name: GIT_SYNC_ROOT
              value: /git
            - name: GIT_SYNC_DEST
              value: app
            - name: GIT_SYNC_ONE_TIME
              value: 'true'
          volumeMounts:
            - name: git-dir
              mountPath: /git
      containers:
        - name: app
          image: {{ .Values.docker.location }}:{{ .Values.docker.tag }}
          args: ['/git/app/src/index.js', '{{ .scraper_name }}', '80']
          ports:
            - containerPort: 80
          volumeMounts:
            - name: git-dir
              mountPath: /git
          envFrom:
            - configMapRef:
                name: {{ .Chart.Name }}-config
            - secretRef:
                name: {{ .Values.global.config.secrets_name }}
            - configMapRef:
                name: {{ .Values.global.config.configs_name }}
          livenessProbe:
            httpGet:
              path: /
              port: 80
            initialDelaySeconds: 60
            periodSeconds: 1800
            failureThreshold: 1
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
              - matchExpressions:
                  - key: scraper
                    operator: Exists
      volumes:
        - name: git-dir
          emptyDir: {}
{{- end }}

{{- define "scraper-template" }}
{{- $deployName := printf "%s-%s" .Chart.Name .scraper_name -}}
{{- $tmpDict := (. | merge (dict "chart_name" $deployName)) -}}
{{- $rollmeValue := include "retrieve-rollme-value" $tmpDict -}}
{{- include "scraper-pod-template" ($tmpDict | merge (dict "scraper_name" .scraper_name "rollmeValue" $rollmeValue )) }}
{{- end }}