{{- $rollmeValue := include "retrieve-rollme-value" . | trim -}}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: minio
  annotations:
    rollme: {{ $rollmeValue }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: minio
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: minio
      annotations:
        rollme: {{ $rollmeValue }}
    spec:
      volumes:
      - name: storage
        hostPath:
          path: '/minio'
      containers:
        - name: minio
          image: minio/minio:latest
          args:
            - server
            - /storage
            - --console-address
            - :9090
          env:
            - name: MINIO_IDENTITY_LDAP_SERVER_ADDR
              value: 'ldap-svc.default.svc.cluster.local:1389'
            - name: MINIO_IDENTITY_LDAP_STS_EXPIRY
              value: '30m'
            - name: MINIO_IDENTITY_LDAP_LOOKUP_BIND_DN
              value: 'cn=admin,dc=example,dc=org'
            - name: MINIO_IDENTITY_LDAP_LOOKUP_BIND_PASSWORD
              value: '{{ mustRegexFind "LDAP_ADMIN_PASSWORD=.+(\\s*|$)" (.Values.global._secrets) | trimPrefix "LDAP_ADMIN_PASSWORD=" | trim }}'
            - name: MINIO_IDENTITY_LDAP_USER_DN_SEARCH_BASE_DN
              value: 'ou=users,dc=example,dc=org'
            - name: MINIO_IDENTITY_LDAP_USER_DN_SEARCH_FILTER
              value: '(&(cn=%s)(objectClass=inetOrgPerson))'
            - name: MINIO_IDENTITY_LDAP_GROUP_SEARCH_BASE_DN
              value: 'ou=users,dc=example,dc=org'
            - name: MINIO_IDENTITY_LDAP_GROUP_SEARCH_FILTER
              value: '(&(objectclass=posixGroup)(memberUid=%s))'
            - name: MINIO_IDENTITY_LDAP_TLS_SKIP_VERIFY
              value: 'on'
            - name: MINIO_IDENTITY_LDAP_SERVER_INSECURE
              value: 'on'
            - name: MINIO_IDENTITY_LDAP_SERVER_STARTTLS
              value: 'off'
          envFrom:
            - secretRef:
                name: minio-secret
          ports:
            - containerPort: 9000
              hostPort: 9000
            - containerPort: 9090
              hostPort: 9090
          volumeMounts:
            - name: storage
              mountPath: '/storage'
      affinity:
          nodeAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
              nodeSelectorTerms:
                - matchExpressions:
                    - key: minio
                      operator: Exists